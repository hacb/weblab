package main

import (
	"html/template"
)

type Post struct {
	Author  string
	Message template.HTML
}

var Xss2Posts []Post
var Xss3Posts []Post

const adminPassword = "foobarbaz"
