package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	Xss2Posts = append(Xss2Posts, Post{Author: "Hugo", Message: "Hack the planet!"})
}

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("templates/*.gohtml")

	r.GET("/", homeHandler)

	r.GET("/xss/:id", xssHandler)
	r.POST("/xss/:id", xssPostHandler)

	r.GET("/sqli/:id", sqliHandler)
	r.POST("/sqli/:id", sqliPostHandler)

	r.GET("/hacker_spotted", hackerSpottedHandler)
	r.POST("/hacker_spotted", hackerSpottedHandler) // ugly but works with redirects

	var err error
	db, err = sql.Open("sqlite3", "./local.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if err = prepDatabase(); err != nil {
		log.Fatal(err)
	}

	if err = createAdminUser(); err != nil {
		log.Fatal(err)
	}

	addr := "localhost:1337"
	fmt.Printf("listening on http://%s\n", addr)
	r.Run(addr)
}

func homeHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "home.gohtml", nil)
}

func xssHandler(c *gin.Context) {
	id := c.Param("id")
	switch id {
	case "1":
		Name := template.HTML(c.Query("name"))
		c.HTML(http.StatusOK, "xss_1.gohtml", Name)
	case "2":
		c.HTML(http.StatusOK, "xss_2.gohtml", Xss2Posts)
	case "3":
		c.HTML(http.StatusOK, "xss_3.gohtml", Xss3Posts)
	default:
		c.HTML(http.StatusNotFound, "not_found.gohtml", nil)
	}
}

func xssPostHandler(c *gin.Context) {
	id := c.Param("id")
	switch id {
	case "2":
		name := c.PostForm("name")
		message := c.PostForm("message")
		Xss2Posts = append(Xss2Posts, Post{Author: name, Message: template.HTML(message)})
		c.HTML(http.StatusOK, "xss_2.gohtml", Xss2Posts)
	case "3":
		name := c.PostForm("name")
		message := c.PostForm("message")

		if strings.Contains(name, "script") || strings.Contains(message, "script") {
			c.Redirect(http.StatusTemporaryRedirect, "/hacker_spotted")
			return
		}
		Xss3Posts = append(Xss3Posts, Post{Author: name, Message: template.HTML(message)})
		c.HTML(http.StatusOK, "xss_3.gohtml", Xss3Posts)
	default:
		c.HTML(http.StatusNotFound, "not_found.gohtml", nil)
	}
}

func hackerSpottedHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "spotted.gohtml", nil)
}

func sqliHandler(c *gin.Context) {
	id := c.Param("id")
	switch id {
	case "1":
		c.HTML(http.StatusOK, "sqli_1.gohtml", nil)
	default:
		c.HTML(http.StatusNotFound, "not_found.gohtml", nil)
	}
}

func sqliPostHandler(c *gin.Context) {
	id := c.Param("id")
	switch id {
	case "1":
		username := c.PostForm("username")
		password := c.PostForm("password")

		if ok := checkCredentials(username, password); !ok {
			c.String(http.StatusBadRequest, "bad credz")
		} else {
			c.HTML(http.StatusOK, "admin_panel.gohtml", nil)
		}
	default:
		c.HTML(http.StatusNotFound, "not_found.gohtml", nil)
	}
}
