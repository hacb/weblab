# Weblab

A small vulnerable web server to get started at XSS and SQL injections. This lab has been developed as part of the Offensive Security 101 course I teach at [Lyon 1 University](https://www.univ-lyon1.fr/en).

## Getting started

Pre-requisite: the Go compiler is required.

* First, clone this repository and enter it:

```
git clone codeberg.org/hacb/weblab.git && cd weblab/
```

* Build the binary:

```
go build .
```

* Then, run the web server you just compiled:

```
./weblab
```

* Finally, you just have to go to [localhost:1337](http://localhost:1337/) in your browser. Happy hacking!

## Disclaimer

A lot of bad coding habits and vulnerabilities have been introduced in the codebase: that's the whole point. This source code should not be considered as a good example.

## License

[MIT](./LICENSE)