package main

import (
	"database/sql"
	"fmt"
)

var db *sql.DB

func prepDatabase() error {
	createTable := `
	CREATE TABLE IF NOT EXISTS users (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		username TEXT,
		password TEXT
	);
`
	_, err := db.Exec(createTable)
	return err
}

func createAdminUser() error {
	createAdmin := fmt.Sprintf(`INSERT INTO users (username,password) VALUES ('admin','%s');`, adminPassword)
	_, err := db.Exec(createAdmin)
	return err
}

func checkCredentials(user, pass string) bool {
	query := fmt.Sprintf(`SELECT id FROM users WHERE username='%s' AND password='%s';`, user, pass)
	r := db.QueryRow(query)

	var id int
	err := r.Scan(&id)
	if err == sql.ErrNoRows {
		return false
	}

	return true
}
